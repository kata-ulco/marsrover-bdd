<?php

declare(strict_types=1);

use Behat\Behat\Context\Context;
use Ulco\Mars;
use Ulco\Rover;
use Webmozart\Assert\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    private ?Rover $rover = null;

    private ?Mars $mars = null;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given there is a rover
     */
    public function thereIsARover()
    {
        $this->rover = new Rover();
    }

    /**
     * @Given there is Mars
     */
    public function thereIsMars()
    {
        $this->mars = new Mars();
    }

    /**
     * @When I land the rover at :arg1, :arg2
     */
    public function iLandTheRoverAt(int $x, int $y)
    {
        $this->mars->land($this->rover, $x, $y);
    }

    /**
     * @Then Rover should be in :arg1, ":arg2
     */
    public function roverShouldBeIn(int $x, int $y)
    {
       $positions = $this->mars->find($this->rover);

       Assert::eq($x, $positions['x']);
       Assert::eq($y, $positions['y']);
    }
}
